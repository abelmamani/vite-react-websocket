import React, { useEffect, useState } from 'react';
import { Conductor } from '../../models';
import { socket } from '../../utils';

export type UserPageProps = {
}

const UserPage: React.FC<UserPageProps> = ({}) => {
  const [conductor, setConductor] = useState<Conductor>({id: '', latitude: '', longitude: ''});

  useEffect(() => {
    socket.on('ubicacionConductor', (message: Conductor) => {
      setConductor(JSON.parse(message));
    });
  }, []);

  return (
    <div className='w-1/3 bg-black p-4 flex flex-col rounded-xl'>
    <h1 className='text-2xl'>Ubicación del Conductor</h1>
	  <h2 className='text-lg mt-2'>ID {conductor.id}</h2>
	  <p className='text-sm mt-2'>Latitude: {conductor.latitude}</p>
	  <p className='text-sm mt-2'>Longitude: {conductor.longitude}</p>
    </div>
  );
};

export default UserPage;
