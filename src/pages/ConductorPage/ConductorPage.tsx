import React, { useState } from 'react';
import { Conductor } from '../../models';
import { socket } from '../../utils';

export type ConductorPageProps = {
}

const ConductorPage: React.FC<ConductorPageProps>  = ({}) => {
	
	const [conductor, setConductor] = useState<Conductor>({id: '', latitude: '', longitude: ''});

  	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
	e.preventDefault();
		const { name, value } = e.target;
		setConductor((prevConductor) => ({...prevConductor, [name]: value, }));
 	 };

  	const enviarUbicacion = () => {
      socket.emit('enviarUbicacion', conductor);
  	};
	return <form onSubmit={enviarUbicacion}  className="w-1/3 flex flex-col gap-4 p-4 bg-black text-white text-center rounded-xl">
		<h1>Enviar ubicacion</h1>
		<input type="text" name="id" required placeholder='ID' onChange={handleChange} className='p-2 text-black'/>
		<input type="text" name="latitude" required placeholder='latitude' onChange={handleChange} className='p-2 text-black'/>
		<input type="text" name="longitude" required placeholder='longitude' onChange={handleChange} className='p-2 text-black'/>
		<button type='submit' className='bg-green-800 p-2 rounded-lg'>Enviar</button>
	</form>
	};

export default ConductorPage;
