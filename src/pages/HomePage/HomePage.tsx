import React, { useEffect, useState } from 'react';

export type HomePageProps = {
}

const HomePage: React.FC<HomePageProps>  = ({}) => {
	const [data, setData] = useState(null);
	const [isLoading, setIsLoading] = useState<boolean>(true);
	const [error, setError] = useState<boolean>(false);
	useEffect(() => {
		const fetchData = async () => {
			try {
			const response = await fetch('http://localhost:3000');
			const jsonData = await response.json();
			setData(jsonData);
			setIsLoading(false);
			} catch (error) {
			setError(true);
			setIsLoading(false);
			}
	 	 };
		 fetchData(); 
  
	}, []); 
  
	if (isLoading) {
	  return <p>Cargando...</p>;
	}
  
	if (error) {
	  return <p>Some error</p>;
	}
  
	return (
		<>
		  <h1 className='text-white text-3xl'>Datos de la API:</h1>
		  <p className='text-white text-lg mt-5'> {JSON.stringify(data, null, 2)}</p>
		</>
	  );
}

export default HomePage;
