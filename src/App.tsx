import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Navbar } from './components';
import React,  { Suspense }from 'react';
const HomePage = React.lazy(() => import('./pages/HomePage/HomePage'));
const UserPage = React.lazy(() => import('./pages/UserPage/UserPage'));
const ConductorPage = React.lazy(() => import('./pages/ConductorPage/ConductorPage'));

function App() { 
  return <>
   <Navbar/>
  <div className='h-screen flex flex-col items-center justify-center bg-gray-900  text-white '>
   <BrowserRouter>
   <Suspense fallback={<div>Cargando...</div>}/>
         <Routes>
         <Route path="/" element={<HomePage/>} />
           <Route path="/users" element={<UserPage/>} />
           <Route path="/conductors" element={<ConductorPage/>} />
         </Routes>
       </BrowserRouter>
 </div>
</>

}

export default App;
