export interface Conductor{
    id: string,
    latitude: string,
    longitude: string
}