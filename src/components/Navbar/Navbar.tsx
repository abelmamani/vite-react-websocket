
export type NavbarProps = {
}

const Navbar: React.FC<NavbarProps>  = ({}) => {
	return <nav className='fixed w-screen bg-black text-white text-xl h-14 flex justify-around  items-center'>
		<a href="/">Home</a>
		<a href="/users">User</a>
		<a href="/conductors">Conductor</a>
		</nav>;
};

export default Navbar;
